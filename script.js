// activity
let students = []
let studentSection = []
function addStudent(student) {
	students.push(student);
	console.log(student + " was added to the student's list.");
}
function countStudents() {
	console.log("There are a total of " + students.length + " students enrolled.");
}
function printStudents() {
	students.forEach(
		function(element){
				console.log(element);
			}
		);
}
function findStudent(student) {
	let foundStudents = []
	students.filter(
		function (stud) {
				// not case sensitive
				if (stud.toLowerCase().includes(student.toLowerCase())) foundStudents.push(stud);
			}
		);
	// no match
	if (foundStudents.length === 0) console.log("No student found with the name " + student);
	// 1 match
	else if(foundStudents.length === 1) console.log(foundStudents[0] + " is an Enrollee");
	// multiple match
	else console.log(foundStudents.concat() + " are enrollees");
}


// stretch goals
function addSection(section) {
	studentSection = students.map(
		function(student){
 			return `${student} - section `+ section;
 			}
		);
	console.log(studentSection);
}
function removeStudent(student) {
	// capitalize
	student =  student.charAt(0).toUpperCase() + student.slice(1);
	// retrieve
	index = students.indexOf(student);
	// remove
	if (index > -1){
		students.splice(index, 1);
		// print
		console.log(student + " was removed from the student's list.");
	}
	else console.log ("No student found with the name " + student);
}